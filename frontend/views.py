from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from frontend.forms import UrlForm
from django.core.urlresolvers import reverse, reverse_lazy
from frontend.models import WebPage, InternalLink
from frontend.models import Report
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth import authenticate
# Create your views here.


class WebPageDetail(DetailView):
    model = WebPage


class ReviewList(ListView):
    model = Report
    paginate_by = 15



class ReviewDetail(DetailView):
    model = Report


class NewTrip(FormView):
    form_class = UrlForm
    template_name = "new_trip.html"
    success_url = reverse_lazy('new-trip')

    def form_valid(self, form):
        form.run_crawler()
        return super(NewTrip, self).form_valid(form)

