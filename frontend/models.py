from django.db import models
import urllib
import logging
from django.core.exceptions import ObjectDoesNotExist
# Create your models here.

from django.db.models import Count
logger = logging.getLogger('frontend.models')

class ExternalLink(models.Model):
    href = models.URLField(null=True)
    text = models.TextField(null=True)
    rel = models.TextField(null=True)


class InternalLink(models.Model):
    #source = models.ForeignKey("WebPage", related_name="source_set", null=True)
    target = models.ForeignKey("WebPage", related_name="target_set", null=True)


    href = models.URLField(null=True)
    text = models.TextField(null=True)
    rel = models.CharField(max_length=200, null=True)


class Heading(models.Model):
    type = models.CharField(max_length=2)
    text = models.TextField()

class Paragraphs(models.Model):

    css_class = models.TextField(null=True, blank=True)
    dom_id = models.CharField(max_length=20, null = True, blank= True)
    content= models.TextField(null = True, blank = True)
    words_count = models.IntegerField()
    characters_count = models.IntegerField()


class Image(models.Model):
    src = models.URLField(max_length=300)
    alt = models.TextField()


class Report(models.Model):
    status = (
        (0, 'Not started'),
        (1, 'In progress'),
        (2, 'Done'),
        (3, 'Failed')
    )


    webpage = models.URLField()
    status = models.IntegerField(choices=status)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    @property
    def time(self):
        return self.update_date - self.create_date

    class Meta:

        ordering=['-create_date']



class WebPage(models.Model):
    report = models.ForeignKey(Report, related_name='rep_set')

    url = models.URLField()
    coding_page = models.CharField(max_length=20, null=True, blank=True)
    title = models.TextField(null=True)
    returned_code = models.IntegerField(null=True)
    last_redirect = models.URLField(null=True)
    internal_links = models.ManyToManyField(InternalLink, null=True)
    external_links = models.ManyToManyField(ExternalLink, null=True)
    paragraphs = models.ManyToManyField(Paragraphs, null =True)
    headings = models.ManyToManyField(Heading)
    img = models.ManyToManyField(Image)

    @property
    def count_headings(self):
        return self.headings.values('type').annotate(count=Count('type'))

    class Meta:
        ordering=['pk']


from CrawlerEngine.Crawler import analyze_done
from django.dispatch import receiver


@receiver(analyze_done)
def save_report(analyze, report_model, status, **kwargs):
    logger.info("Saving report into database")
    visited_pages = analyze

    if status != 2:
        logger.error("Error occured, changing report status to 'Fail'")
        report_model.status = 3
        report_model.save()

        return None
    #print visited_pages
    #print len(visited_pages)
    #return None

    logger.info("Creating webpage objects")
    for page in visited_pages.keys():


        page_model = WebPage(url=page, report=report_model)
        page_model.save()


    for page in visited_pages.keys():
        logger.info("Saving %s" % page)
        current_page = visited_pages[page]
        page_model = WebPage.objects.get(url=page, report=report_model)

        page_model.title = current_page.get('title',None)
        page_model.coding_page = current_page.get('coding_page',None)
        #print "pracuje nad strona"
        logger.info("Saving images information")
        for img in current_page.get('img',()):
            #print "zapisuje obrazek"
            logger.debug("obrazek: %s" % (img.get('src','')))
            try:
                page_model.img.create(src=unicode(img.get('src',None)), alt=unicode(img.get('alt',None)))
            except Exception as ex:
                logger.critical("Img not saved %(src)s %(alt)s" % img)
                logger.critical(ex)



        logger.info("Saving headings information")
        for head in current_page.get('heads',()):
            #print "zapisuje heading"
            try:
                page_model.headings.create(type=unicode(head.get('tag')), text=unicode(head.get('value')))
            except Exception as ex:
                logger.critical("heading not saved %(tag) , %(value)s" % head)
                logger.critical(ex)
        logger.info("Saving paragraphs information")
        for paragraph in current_page.get('paragraphs',()):
            try:
                page_model.paragraphs.create(dom_id=paragraph.get('id'), words_count=paragraph.get('words_count'), characters_count=paragraph.get('characters_count'), css_class=paragraph.get('css_class'),content=paragraph.get('content'))
            except Exception as ex:
                logger.critical("paragraph not saved")
                logger.critical(ex)
        logger.info("Saving internal links information")
        for internal in current_page.get('internal_links',()):
            #print "zapisuje internalowy link %s" % internal['target']
            try:
                target_site = WebPage.objects.get(url=unicode(internal.get('target','')[:200]), report=report_model)
            except ObjectDoesNotExist as ex:
                target_site = None

            try:
                page_model.internal_links.create(target=target_site, href=unicode(internal.get('href')[:200]),rel=unicode(internal.get('rel','')[:200]), text=unicode(internal.get('text','')[:200]))
            except Exception as ex:
                logger.critical("Cannot add internal links %(href)s %(text)s" % internal)
                logger.critical(ex)


        logger.info("Saving external links information")
        for external in current_page.get('external_links',()):
            #print "zapisuje externalowy %s" % external['href']
            try:
                page_model.external_links.create(href=unicode(external.get('href',''))[:200], text=unicode(external.get('text')),
                                             rel=unicode(external.get('rel')))
            except Exception as ex:
                logger.critical("Cannot add external link %(href)s %(text)s" % external)
                logger.critical(ex)


        page_model.returned_code = current_page.get('returned_code',-1)
        page_model.last_redirect = current_page.get('last_redirect',None)
        #print "koncze jedna strone, ide w kolejna"
        page_model.save()
        #print "-------------------------------------------------"
    report_model.status = status
    report_model.save()
    logger.info("Report saved")





