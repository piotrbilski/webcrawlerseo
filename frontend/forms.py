from django import forms
from CrawlerEngine.Crawler import CrawlerThread
from frontend.models import Report

class UrlForm(forms.Form):
    url = forms.URLField()
    pages = forms.IntegerField(initial=50)
    timeout = forms.IntegerField(initial=5)

    def run_crawler(self):
        url = self.cleaned_data['url']
        pages = self.cleaned_data['pages']
        timeout = self.cleaned_data['timeout']
        rep = Report(webpage=url, status=0 )
        rep.save()
        crawler = CrawlerThread(rep, url, max_loop=pages, timeout=timeout)
        crawler.start()
        rep.status = 1
        rep.save()
        # crawler.visit_page(url)
        #return crawler.visited_pages