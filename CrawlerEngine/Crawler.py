__author__ = 'piotr'

from bs4 import BeautifulSoup
import requests
import threading
import re
from argparse import ArgumentParser
from urlparse import urlparse
from urlparse import urljoin
import django.dispatch
import logging
logger = logging.getLogger('CrawlerEngine.Crawler')

args = ArgumentParser(description="Web spider will visit webpage and collect desired information.")
args.add_argument("www", help="Page address")
args.add_argument("--max_pages", help="Max number of visited pages", default=100)

#declare signal to send to django - consider moving this to another file?
analyze_done = django.dispatch.Signal(providing_args=['analyze', 'report_model', 'status'])

class Crawler(object):
    visited_pages = {}
    site_map = {}
    visited_links = []
    counter = 0
    links_queue = []

    def __init__(self, url, max_loop=100, timeout=5):
        '''

        Initialize base variables.

        :param url: site to check
        :param max_loop: how many pages crawler can visit
        :return: None


        '''
        super(Crawler, self).__init__()
        self.timeout=timeout
        self.visited_pages={}
        self.site_map={}
        self.visited_links=[]
        self.links_queue=[]
        self.counter = 0
        self.max_loop = max_loop
        self.start_page = url
        self.main_domain = urlparse(url).netloc
        self.external_link_pattern = re.compile(r"https?://(?!(www\.)?%s)" % self.main_domain)
        logger.info("Research for %s started..." % url)


    def handle_img_tags(self, soup):
        '''
        Parse 'soup' for img tags.

        :param soup: page content
        :return output: [{'src': ... , 'alt': ...,}]

        '''

        imgs = soup.find_all('img')
        output = [dict(src=img.get('src'), alt=img.get('alt', None)) for img in imgs]
        return output

    def handle_p_tags(self,soup):
        '''
        Parse page looking for paragraphs. Count number of used words and letters.

        :param soup:
        :return:
        '''

        output = []

        paragraphs = soup.find_all('p')
        output.extend([{"css_class" : paragraph.get('class', None),
                           "id":paragraph.get("id",None),
                           "words_count": len(paragraph.text.split()),
                           "characters_count": len(paragraph.text),
                           "content":" ".join(paragraph.text.split()[:50])} for paragraph in paragraphs ])



        return output

    def handle_h_tags(self, soup, max_level=8):
        '''
        Parse 'soup' for h-eadings tags.

        :param soup: page content
        :param max_level: maximum of H level tag
        :return output: [ {'tag': ... , 'value': ...} ]
        '''

        output = []

        for level in range(1, max_level):
            titles = soup.find_all('h%d' % level)
            output.extend([{'tag': head.name, 'value': head.text.strip()} for head in titles])

        return output

    def handle_title_tag(self, soup):
        '''

        Parsing 'soup' for page title.

        :param soup: page content
        :return title: title of document

        '''
        try:
            title = soup.title.text.strip() or "No title"
        except:
            title = None
            logger.warning("Page don't have title tag" )
        return title

    def handle_encoding(self, soup):
        """
        Method is looking for declared charset on the webpage.

        :param soup: page content
        :return charset: charset declared in meta tags or None
        """
        charset = None
        meta_tags = soup.find_all("meta")
        for meta in meta_tags:

            #first look for meta tag with charset parameter
            charset = meta.get('charset',None)
            #if nothing found
            if not charset:
                #look for meta tag with content parameter and try to get charset using regular expression
                charset = meta.get('content')
                charset = re.match(".*charset=(?P<charset>.*)", charset)
                try:
                    #if found, break loop and return value
                    return charset.group('charset')
                except Exception:
                    logger.warning("Page don't have declared charset")
                    pass

        return charset


    def handle_link_tag(self, soup, parent_url=""):
        '''

        Parsing 'soup' for links.  It distinguishes links internal and external so it returns tuple with two arrays.

        :param soup: page content
        :return internal_links, external_links: tuple of arrays
        '''

        links = soup.find_all('a')
        internal_links = []
        external_links = []
        for link in links:

            href = link.get('href', "")

            # check does url is relative or absolute type
            if not urlparse(href).scheme:
                target = urljoin(parent_url, href)
            else:
                target = href

            link_dict = {"target": target, "href": link.get('href', '#'), "rel": link.get("rel", ""),
                         "text": link.text}

            if self.external_link_pattern.match(link_dict['href']):
                external_links.append(link_dict)
            else:
                internal_links.append(link_dict)

        return internal_links, external_links


    def visit_page(self, url, parent_url=None):
        """

        :param url: address of webpage to parse
        :param parent_url: if current page is subpage of parent

        :return: void
        """

        #if parent url doesn't exist, set url as parent
        parent_url = parent_url or url

        #if page was already checked,
        if url in self.visited_links:
            return None

        #incrase counter, break if equal or higher than declared max value
        self.counter += 1
        if self.counter >= self.max_loop:
            return None


        try:
            page = requests.get(url, timeout=self.timeout)

            logger.info("Lets go visit %s" %url)
        except requests.exceptions.InvalidSchema as Ex:
            logger.warning("Cannot open url %s " % Ex)
            return None
        except requests.exceptions.MissingSchema as Ex:
            logger.info("Incorrect URL, trying build new one %s" % url)
            # In case, when provided path is relative, we need to join it with parent url.
            # urljoin is smart method, which replaces last element if needed
            url = urljoin(parent_url, url)
            return self.visit_page(url, parent_url)
        except requests.ConnectionError as ex:
            logger.error('Cannot open connection to %s : %s' % (url, ex))
            current_dict = self.visited_pages[url] = {}
            current_dict['title'] = "Couldn't reach URL"
            return None

        # checking type of downloaded document. We are interrested only in html documents, so we're looking for 'text' in content-type
        if "text" not in page.headers['content-type']:
            logger.warning(page.headers['content-type'])
            logger.warning("Page is not html document %s" % url)
            return None

        # creating structure for current page information
        current_dict = self.visited_pages[url] = {}
        try:
            current_dict['returned_code'] = page.history[-1].status_code
            current_dict['last_redirect'] = url = page.url


        except:
            logger.error("No redirection found? ")
            current_dict['returned_code'] = page.status_code

        # ok, so lets start analyze the page content
        content = page.text
        soup = BeautifulSoup(content)

        #it's time to check declared code page. if it's different than sent in request headers, we need to set new encoding and download page one more time
        current_dict['coding_page'] = self.handle_encoding(soup) or page.encoding
        if current_dict['coding_page'] != page.encoding:
            logger.info("Declared charset is different than in http request. Re-download document %s" % url)
            page.encoding = current_dict['coding_page']
            content = page.text
            soup = BeautifulSoup(content)


        current_dict['heads'] = self.handle_h_tags(soup)
        current_dict['title'] = self.handle_title_tag(soup)
        current_dict["img"] = self.handle_img_tags(soup)
        current_dict["paragraphs"] = self.handle_p_tags(soup)
        current_dict['internal_links'], current_dict['external_links'] = self.handle_link_tag(soup, url)

        self.visited_links.append(url)

        #extend list with urls to visit
        self.links_queue.extend([i['target'] for i in current_dict['internal_links']])






class CrawlerThread(Crawler, threading.Thread):
    def __init__(self, report_model ,*args, **kwargs):

        super(CrawlerThread, self).__init__(*args, **kwargs)
        self.report_model = report_model


    def run(self):
        try:
            self.visit_page(self.start_page)
            while self.links_queue and self.counter < self.max_loop:
                self.visit_page(self.links_queue.pop(0))

            analyze_done.send(sender=self.__class__, analyze=self.visited_pages, report_model=self.report_model,
                              status=2)
        except Exception as ex:
            logger.error("Something goes wrong, %s" % ex)
            analyze_done.send(sender=self.__class__, analyze={}, report_model=self.report_model, status=3)

            # something goes wrong, so change status to failed

