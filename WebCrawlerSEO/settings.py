"""
Django settings for WebCrawlerSEO project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from numpy.core.defchararray import _use_unicode
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g2qg7qza@uo3plj@vk)gt0v9-+!69j88#l3p2ow+e!@pexh7k0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'frontend',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'WebCrawlerSEO.urls'

WSGI_APPLICATION = 'WebCrawlerSEO.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
LOGIN_URL='/'
LOGIN_REDIRECT_URL='/reports_list/'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'crawler',
        'USER': 'seo',
        'HOST': 'localhost',
        'PASSWORD': 'm15arta13',
        'OPTIONS': {'charset': 'utf8', "use_unicode": True},

    }
}

TEMPLATE_DIRS = ( os.path.join(BASE_DIR, 'templates'), )
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'), )

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'pl-PL'

TIME_ZONE = 'CET'

USE_I18N = True

USE_L10N = True

USE_TZ = True




# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters':{
        'verbose' : {
            'format':'%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple' : {
            'format':'%(levelname)s %(asctime)s %(module)s %(process)d %(message)s'
        }
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when' : 'midnight',
            'formatter' : 'simple',
            'filename': 'logs/WebCrawler.log',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'CrawlerEngine.Crawler':{
            'handlers' : ['file'],
            'level' : 'DEBUG',
            'propagate': True
        },
         'frontend.models':{
            'handlers' : ['file'],
            'level' : 'DEBUG',
            'propagate': True
        }
    },
}