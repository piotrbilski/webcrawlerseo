from django.conf.urls import patterns, include, url
from frontend.views import NewTrip, ReviewList, ReviewDetail, WebPageDetail
from django.contrib import admin
from django.contrib.auth.views import login, logout_then_login

from django.contrib.auth.decorators import login_required
admin.autodiscover()

urlpatterns = patterns('',
                       url('^$', login , {'redirect_field_name':'reports-list'}, name='login'),
                       url('^logout', logout_then_login ,name='logout'),
                       url('^new_trip/$', login_required(NewTrip.as_view()), name='new-trip'),
                       url('^reports_list/$', login_required(ReviewList.as_view()), name='reports-list'),
                       url('^review_detail/(?P<pk>\d+)', login_required(ReviewDetail.as_view()), name='report-detail'),
                       url('^webpage_detail/(?P<pk>\d+)', login_required(WebPageDetail.as_view()), name='webpage-detail'),
                       #url('', TemplateView.as_view(template_name="crawler.html"), name='index'),

                       # Examples:
                       # url(r'^$', 'WebCrawlerSEO.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
)
